#!/usr/bin/env python

import csv
import json
import sys

params = json.load(sys.stdin)
csvfile = params['csvfile']
noop = params.get('_noop', False)
exitcode = 0

def make_error(msg):
  error = {
      "_error": {
          "kind": "file_error",
          "msg": msg,
          "details": {},
      }
  }
  return error

result = {}
try:
  with open(csvfile, 'r', encoding='utf-8') as c:
    myreader = csv.DictReader(c)
    for rows in myreader:
      key = rows['No']
      result[key] = rows

except Exception as e:
  exitcode = 1
  result = make_error("Could not open file %s: %s" % (csvfile, str(e)))

print(json.dumps(result))
exit(exitcode)

