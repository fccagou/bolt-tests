#!/bin/bash

infos="$(ip r s  | grep ^default  | awk '{ print $3" "$5 }')"
dev="${infos//* }"
cat <<EOF
{ "dev": "${dev}", "ip": "$(ip a s ${dev}  | grep 'inet ' | awk '{ print $2 }'|cut -d/ -f1)", "gw": "${infos// *}" }
EOF

