#!/usr/bin/env python

import csv
import json
import sys

params = json.load(sys.stdin)
noop = params.get('_noop', False)
data = params['data']

exitcode = 0

def make_error(msg):
  error = {
      "_error": {
          "kind": "file_error",
          "msg": msg,
          "details": {},
      }
  }
  return error

result = {}
try:
    for r in data:
        result[r] = data[r]['user']


except Exception as e:
  exitcode = 1
  result = make_error("Error %s" % (str(e)))

print(json.dumps(result))
exit(exitcode)

